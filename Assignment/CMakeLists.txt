cmake_minimum_required(VERSION 3.0)

project(assignment_latex_template VERSION 0.1.0)

include(cmake/PlantUML.cmake)
include(cmake/UseLATEX.cmake)


set(inputs structure.tex)

file(GLOB_RECURSE listings "Listings/*")
foreach(path ${listings})
    get_filename_component(rpath ${path} NAME)
    set(inputs ${inputs} "Listings/${rpath}")
endforeach()

file(GLOB_RECURSE figures "Figures/*")
foreach(path ${figures})
    get_filename_component(rpath ${path} NAME)
    set(inputs ${inputs} "Figures/${rpath}")
    add_diagram("Figures/${rpath}")
endforeach()



add_latex_document(template_assignment.tex
    INPUTS ${inputs}
    IMAGE_DIRS ${CMAKE_BUILD_RPATH}/Figures
    DEPENDS plantuml-diagrams
)