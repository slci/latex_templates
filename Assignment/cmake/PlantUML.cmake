find_program(PLANTUML plantuml
             PATH_SUFFIXES PlantUML plantuml Plantuml 
             PATHS /usr/share /usr/local/share/ /usr/local/bin /usr/bin)

if(NOT PLANTUML)
    message(FATAL_ERROR "PlantUML found? - NO")
else()
    message(STATUS "PlantUML found? - YES")
endif()


add_custom_target(plantuml-diagrams)

#
# Create a PlantUML target that renders a PNG image.
#
function(add_diagram source)

    # Diagram source file basename used to create output file name.
    get_filename_component(output_name ${source} NAME_WE)
    get_filename_component(output_dir ${source} DIRECTORY)

    set(output_path "${CMAKE_BINARY_DIR}/${output_dir}/${output_name}.png")

    add_custom_command(
        OUTPUT ${output_path}
        COMMAND ${PLANTUML} -o ${CMAKE_BINARY_DIR}/${output_dir} -tpng ${CMAKE_CURRENT_SOURCE_DIR}/${source}
        MAIN_DEPENDENCY ${CMAKE_CURRENT_SOURCE_DIR}/${source}
        COMMENT "Rendering diagram ${output_dir}/${output_name}.png"
    )

    add_custom_target(${output_name}
        DEPENDS ${output_path}
    )

    add_dependencies(plantuml-diagrams ${output_name})

endfunction()